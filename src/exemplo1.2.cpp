//============================================================================
// Name        : 2.cpp
// Author      : Ricardo Santos
// Version     :
// Copyright   : Free for reading and distributing
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#define NUMERO_POSICOES 10000000

using namespace std;

int main() {

	// Declara as vari�veis que ser�o utilizadas no c�digo
	float *vetor1, *vetor2, *vetor3;

	// Aloca a mem�ria para as vari�veis que ser�o utilizadas
	vetor1 = new float[NUMERO_POSICOES];
	vetor2 = new float[NUMERO_POSICOES];
	vetor3 = new float[NUMERO_POSICOES];

	// Verifica por erro de aloca��o da mem�ria
	if( !vetor1 || !vetor2 || !vetor3 )
		cout << "Mem�ria n�o alocada, erro de aloca��o.";

	// Salva o tempo inicial para contabiliza��o de tempo de inicializa��o dos vetores
	time_t inicial = clock();

	// ENQUANTO i for menor que o n�mero de posi��es do vetor
	for( int i = 0; i < NUMERO_POSICOES; i++)
	{
		// Inicializa a posi��o i do vetor 1 com um n�mero aleat�rio entre 0.0 e 1.0
		vetor1[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

		// Inicializa a posi��o i do vetor 2 com um n�mero aleat�rio entre 0.0 e 1.0
		vetor2[i] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	}
	// FIM ENQUANTO

	// Salva o tempo final da inicializa��o dos vetores
	time_t final = clock();

	// Calcula o tempo para a inicializa��o dos vetores
	time_t elapsedInicializacao = final-inicial;

	// Salva o tempo inicial para contabiliza��o da soma dos vetores
	inicial = clock();

	// ENQUANTO i for menor que o n�mero de posi��es do vetor
	for( int i = 0; i < NUMERO_POSICOES; i++)
	{
		// O vetor 3 em sua posi��o i recebe o valor da soma do i-�simo elemento dos vetores 1 e 2
		vetor3[i] = vetor1[i] + vetor2[i];
	}

	// Salva o tempo final da soma dos vetores
	final = clock();

	// Calcula o tempo de soma dos vetores
	time_t elapsedSoma = final-inicial;

	// Imprime na tela os tempos para o usu�rio
	cout << "Tempo de inicializa��o dos vetores: " << (float(elapsedInicializacao)/CLOCKS_PER_SEC) << " segundos." << endl;
	cout << "Tempo de soma dos vetores: " << (float(elapsedSoma)/CLOCKS_PER_SEC) << " segundos." << endl;
	return 0;
}
